
public class VerticalCircle extends Circle {
	private double speed;
	private int direction;
	private double minY;
	private double maxY;
	
	public VerticalCircle(double x, double y, double radius, double minY, double maxY) {
		super(x, y, radius);
		speed = 0.05;
		direction = 1;
		this.minY = minY;
		this.maxY = maxY;
	}
	
	protected void updateY() {
		y += direction * speed;
		if (y > maxY) {
			direction = -1;
		}
		else if (y < minY) {
			direction = 1;
		}
	}
	
	public void draw() {
		updateY();
		StdDraw.setPenColor(color);
		StdDraw.filledCircle(x, y, radius);
	}
}
