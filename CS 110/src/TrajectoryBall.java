
import javax.swing.JOptionPane;

public class TrajectoryBall {
	public static void main(String[] args) {
		final double GRAVITY = 9.8;
		final int X_MIN = 0;
		final int X_MAX = 18;
		final int Y_MIN = -13;
		final int Y_MAX = 5;
		double xPos = 0;
		double yPos = 0;
		double time = 0;
		double radians = 0;
		double velocity = 10;
		double xVelocity = 0;
		double yVelocity = 0;

		radians = Math.toRadians(Double.parseDouble(JOptionPane
				.showInputDialog("Please enter an angle for trajectory.")));
		xVelocity = velocity * Math.cos(radians);
		yVelocity = velocity * Math.sin(radians);

		StdDraw.setXscale(X_MIN, X_MAX);
		StdDraw.setYscale(Y_MIN, Y_MAX);

		while (xPos <= X_MAX && yPos >= Y_MIN) {
			time += 0.01;

			xPos = xVelocity * time;
			yPos = (yVelocity * time) - (0.5 * GRAVITY * Math.pow(time, 2));

			StdDraw.setPenColor(StdDraw.WHITE);
			StdDraw.filledSquare(xPos, yPos, 1);

			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.filledCircle(xPos, yPos, 0.3);

			StdDraw.show(10);
		}
	}
}