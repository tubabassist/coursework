import java.awt.Color;

public class State {
	private String name;
	private double[] longitude;
	private double[] latitude;
	private double centerX;
	private double centerY;
	private Color color;

	public State(String stateName, double[] x, double[] y) {
		name = stateName;
		longitude = x;
		latitude = y;
		color = Color.blue;
		centerX = calculateCenter(longitude);
		centerY = calculateCenter(latitude);
	}

	public void draw() {
		StdDraw.setPenColor(color);
		StdDraw.filledPolygon(longitude, latitude);
		StdDraw.setPenColor(Color.white);
		StdDraw.text(centerX, centerY, name);
	}

	public String getName() {
		return name;
	}

	public void setColor(Color newColor) {
		color = newColor;
	}

	public void setColorValue(double newValue) {
		color = new Color(0, 0, (int) (newValue * 255));
	}

	private double calculateCenter(double[] array) {
		double min = array[0];
		double max = array[0];

		for (int i = 0; i < array.length; i++) {
			if (min > array[i])
				min = array[i];
			if (max < array[i])
				max = array[i];
		}

		return (max + min) / 2;
	}
}
