

import java.awt.Color;

public class Circle {
	protected double x;
	protected double y;
	protected double radius;
	protected Color color;
	private int directionX;
	private int directionY;

	public Circle(double newX, double newY, double newRadius) {
		x = newX;
		y = newY;
		radius = newRadius;
		color = Color.BLACK;
		directionX = 1;
		directionY = 1;
	}

	public Circle(int newX, int newY, double newRadius) {
		x = newX;
		y = newY;
		radius = newRadius;
		color = Color.BLACK;
		directionX = 1;
		directionY = 1;
	}

	public void draw() {
		StdDraw.setPenColor(color);
		StdDraw.filledCircle(x, y, radius);
	}

	public void updateRadius(double delta) {
		radius += delta;
		if (radius < 0)
			radius = 0;
	}

	public double getRadius() {
		return radius;
	}

	public void setColor(Color newColor) {
		color = newColor;
	}

	public void updateX(double delta, double minX, double maxX) {
		x += directionX * delta;

		if (x + radius >= maxX) {
			x = maxX - radius;
			directionX = -1;
		}

		if (x - radius <= minX) {
			x = minX + radius;
			directionX = 1;
		}
	}

	public void updateY(double delta, double minY, double maxY) {
		y += directionY * delta;

		if (y + radius >= maxY) {
			y = maxY - radius;
			directionY = -1;
		}

		if (y - radius <= minY) {
			y = minY + radius;
			directionY = 1;
		}
	}
}