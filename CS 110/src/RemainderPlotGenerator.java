import java.util.Scanner;

public class RemainderPlotGenerator {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 0;
		int remainder = 0;
		final int MIN_X = 0;
		final int MIN_Y = 0;

		System.out.println("Please enter a number.");
		number = input.nextInt();

		StdDraw.setXscale(MIN_X, number + 1);
		StdDraw.setYscale(MIN_Y, number + 1);

		for (int i = 1; i <= number; i++) {
			for (int j = 1; j <= number; j++) {
				remainder = i % j;
				StdDraw.setPenColor(0, 0, (10 * remainder) % 256);
				StdDraw.filledCircle(j, number - (i - 1), 0.4 * remainder
						/ (number - 1.0));
			}
			System.out.println();
		}
		input.close();
	}
}