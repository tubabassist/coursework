import java.awt.Color;
import java.util.Scanner;

public class UserColorfulCirclesPlotter {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double minRadius = 0;
		double maxRadius = 0;
		int nrCircles = 0;
		double radius = 0;
		int remainder = 0;
		
		System.out.println("Please enter a minimum value for the radius.");
		minRadius = input.nextDouble();
		System.out.println("Please enter a maximum value for the radius.");
		maxRadius = input.nextDouble();
		System.out.println("Please enter the number of circles to generate.");
		nrCircles = input.nextInt();
		
		for (int i = 1; i <= nrCircles; i++) {
			remainder = i % 3;
			switch (remainder) {
			case 0:
				StdDraw.setPenColor(Color.BLACK);
				break;
			case 1:
				StdDraw.setPenColor(Color.BLUE);
				break;
			case 2:
				StdDraw.setPenColor(Color.YELLOW);
				break;
			}
			radius = Math.random() * (maxRadius - minRadius) + maxRadius;
			StdDraw.circle(Math.random(), Math.random(), radius);
		}
		input.close();
	}
}