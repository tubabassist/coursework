
public class HorizontalCircle extends VerticalCircle{
	private double speed;
	private int direction;
	private double minX;
	private double maxX;
	
	public HorizontalCircle(double x, double y, double radius, double minY, double maxY, double minX, double maxX) {
		super(x, y, radius, minY, maxY);
		speed = 0.05;
		direction = 1;
		this.minX = minX;
		this.maxX = maxX;
	}
	
	private void updateX() {
		x += direction * speed;
		if (x > maxX) {
			direction = -1;
		}
		else if (x < minX) {
			direction = 1;
		}
	}
	
	public void draw() {
		updateX();
		updateY();
		StdDraw.setPenColor(color);
		StdDraw.filledCircle(x, y, radius);
	}
}
