import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class USAMapGenerator {
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("states.txt");
		Scanner input = new Scanner(file);
		double minLong = 0;
		double maxLong = 0;
		double minLat = 0;
		double maxLat = 0;
		int nrStates = 0;
		State state = null;
		State[] states = null;
		final int WIDTH = 1366;
		
		minLong = input.nextDouble();
		maxLong = input.nextDouble();
		minLat = input.nextDouble();
		maxLat = input.nextDouble();
		
		StdDraw.setCanvasSize(WIDTH, (int)((WIDTH / (maxLong - minLong) * (maxLat - minLat))));
		
		StdDraw.setXscale(minLong, maxLong);
		StdDraw.setYscale(minLat, maxLat);
		
		nrStates = input.nextInt();
		input.nextLine();
		states = new State[nrStates];
		
		for (int i = 0; i < nrStates; i++) {
			state = readState(input);
			state.setColorValue(Math.random());
			states[i] = state;
		}
		
		boolean blink = true;
		while (true) {
			StdDraw.clear(Color.gray);
			for (int i = 0; i < states.length; i++) {
				states[i].draw();
				if (states[i].getName().equalsIgnoreCase("West Virginia")) {
					if (blink) {
						blink = false;
						states[i].setColor(Color.yellow);
					}
					else {
						blink = true;
						states[i].setColor(Color.blue);
					}
				}
			}
			
			StdDraw.show(200);
		}
	}
	
	public static State readState(Scanner input) {
		String stateName = null;
		int nrCoord = 0;
		double[] longitude = null;
		double[] latitude = null;
		State state = null;
		
		stateName = input.nextLine();
		nrCoord = input.nextInt();
		longitude = new double[nrCoord];
		latitude = new double[nrCoord];
		
		for (int j = 0; j < nrCoord; j++) {
			longitude[j] = input.nextDouble();
			latitude[j] = input.nextDouble();
		}
		input.nextLine();
		
		state = new State(stateName, longitude, latitude);
		return state;
	}
}
