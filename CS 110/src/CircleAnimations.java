
import java.awt.Color;

public class CircleAnimations {
	public static void main(String[] args) {
		final double MIN_X = -1;
		final double MAX_X = 1;
		final double MIN_Y = -1;
		final double MAX_Y = 1;
		Circle circle1 = new Circle(0, 0, .1);
		Circle circle2 = new Circle(0, 0, .1);
		Circle circle3 = new Circle(0, 0, .1);
		Circle circle4 = new Circle(0, 0, .1);
		Circle circle5 = new Circle(0, 0, .1);

		StdDraw.setXscale(MIN_X, MAX_X);
		StdDraw.setYscale(MIN_Y, MAX_Y);

		while (true) {
			StdDraw.clear();
			circle1.draw();
			circle2.draw();
			circle3.draw();
			circle4.draw();
			circle5.draw();

			circle2.updateX(0.01, MIN_X, MAX_X);
			circle2.updateY(0.05, MIN_Y, MAX_Y);
			circle2.updateRadius(Math.random() * 0.1 - 0.05);

			circle1.setColor(Color.GREEN);
			if (circle1.getRadius() > 0.01) {
				circle1.updateRadius(-0.01);
			} else {
				circle1.updateRadius(1);
			}

			StdDraw.show(20);
		}
	}
}