import java.awt.Color;

public class Animation {
	public static void main(String[] args) {
		final double MAX_X = 1;
		final double MIN_X = -1;
		final double MAX_Y = 1;
		final double MIN_Y = -1;
		Circle[] circles = new Circle[5];
		double startX = -0.8;
		
		StdDraw.setXscale(MIN_X, MAX_X);
		StdDraw.setYscale(MIN_Y, MAX_Y);
		
		for (int i = 0; i < circles.length; i++) {
			if (i % 2 == 0){
				circles[i] = new GrowCircle(startX, 0, Math.random() * 0.2, Color.blue, Color.yellow);
			}
			else {
				circles[i] = new HorizontalCircle(startX, 0, Math.random() * 0.2,
						MIN_Y, MAX_Y, MIN_X, MAX_X);
			}
			startX += 0.4;
		}
		
		while(true) {
			StdDraw.clear();
			
			for (int i = 0; i < circles.length; i++) {
				circles[i].draw();
			}
			
			StdDraw.show(20);
		}
	}
}
