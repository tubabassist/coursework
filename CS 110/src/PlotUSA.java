

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PlotUSA {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner inputFile = null;
		double xCoordinate = 0;
		double yCoordinate = 0;
		final String FILENAME = "USA.txt";
		final int MIN_X = 669905;
		final int MAX_X = 1244962;
		final int MIN_Y = 247205;
		final int MAX_Y = 700000;
			
		inputFile = new Scanner(new File(FILENAME));
		
		StdDraw.setXscale(MIN_X, MAX_X);
		StdDraw.setYscale(MIN_Y, MAX_Y);
		
		while (inputFile.hasNextLine()) {
			xCoordinate = inputFile.nextDouble();
			yCoordinate = inputFile.nextDouble();
			StdDraw.point(xCoordinate, yCoordinate);
		}
		inputFile.close();
	}
}
