package jdavis31.cs110.inventoryManager;

public class Product {
	private int productID;
	private String name;
	private String description;
	private String size;
	private double price;
	private int soldYTD;
	
	public Product() {
		productID = 0;
		name = null;
		description = null;
		size = null;
		price = 0;
		soldYTD = 0;
	}
	
	public Product(int productID, String name, String size, double price, int soldYTD, String description) {
		this.productID = productID;
		this.name = name;
		this.description = description;
		this.size = size;
		this.price = price;
		this.soldYTD = soldYTD;
	}
	
	public Product(Product anotherProduct) {
		this.productID = anotherProduct.getProductID();
		this.name = anotherProduct.getName();
		this.description = anotherProduct.getDescription();
		this.size = anotherProduct.getSize();
		this.price = anotherProduct.getPrice();
		this.soldYTD = anotherProduct.getSoldYTD();
	}
	
	public int compareTo(Product anotherProduct) {
		return anotherProduct.getName().compareTo(name);
	}

	public String getDescription() {
		return description;
	}
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getProductID() {
		return productID;
	}
	
	public String getSize() {
		return size;
	}
	
	public int getSoldYTD() {
		return soldYTD;
	}

	public void setDescription(String description) {
		if (description.length() > 25) {
			description = description.substring(0, 24);
		}
		this.description = description;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setProductID(int productID) {
		this.productID = productID;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setSoldYTD(int soldYTD) {
		this.soldYTD = soldYTD;
	}
	
	public String toString() {
		return String.format("%-10d | %-20s | %-25s | %-8s | $%-8.2f | %-8d", productID, name, description, size, price, soldYTD);
	}
}
