package jdavis31.cs110.inventoryManager;

import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.plaf.FontUIResource;

public class InventoryManager extends JFrame {
	public InventoryManager() {
	}
	private static final long serialVersionUID = 1L;
	private static JFrame inventoryManager;
	private static JButton displayByIDButton;
	private static JButton displayByNameButton;
	private static JButton displayByMaxButton;
	private static JButton displayByMinButton;
	private static JButton displayBetweenButton;
	private static JButton addButton;
	private static JButton setPriceButton;
	private static JButton setSalesButton;
	private static JButton displayAllButton;
	private static JButton quitButton;
	private static ArrayList<Product> products = new ArrayList<Product>();

	public static void main(String[] args) {
		String filename = null;
		Scanner inputFile = null;
		//boolean invalidInput = false;
		
		javax.swing.UIManager.put("OptionPane.messageFont", new FontUIResource(
				new Font("Monospaced", Font.PLAIN, 16)));
		createAndShowGUI("Inventory Manager");
		
		//do {
		//	invalidInput = false;
		//	filename = JOptionPane
		//			.showInputDialog("Please enter the inventory data filename:");
			try {
		//		inputFile = new Scanner(new File(filename));
				inputFile = new Scanner(new File("inventoryData.txt"));
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(inventoryManager, String.format("File '%s' was not found.", filename), "Error",
						JOptionPane.ERROR_MESSAGE);
		//		invalidInput = true;
			} catch (NullPointerException e) {
				System.exit(0);
			}
		//} while (invalidInput);
		
		do {
			Product temp = new Product();
			temp.setProductID(inputFile.nextInt());
			temp.setName(inputFile.next().trim());
			temp.setSize(inputFile.next().trim());
			temp.setPrice(inputFile.nextDouble());
			temp.setSoldYTD(inputFile.nextInt());
			temp.setDescription(inputFile.nextLine());
			if (temp.getDescription().length() > 25)
				temp.setDescription(temp.getDescription().substring(0, 24)
						.trim());
			products.add(temp);
		} while (inputFile.hasNextLine());

		inputFile.close();
		sortProducts();

		while (true) {
			try {
				switch (Integer
						.parseInt(JOptionPane
								.showInputDialog("Main Menu\n"
										+ " 1) Display product by id.\n"
										+ " 2) Display product by name.\n"
										+ " 3) Display all products with a maximum price of:\n"
										+ " 4) Display all products with a minimum price of:\n"
										+ " 5) Display all products consting between:\n"
										+ " 6) Add new product.\n"
										+ " 7) Set product price.\n"
										+ " 8) Set product sales.\n"
										+ " 9) Display all products.\n"
										+ "10) Quit."))) {
				case 1:
					displayByID();
					break;
				case 2:
					displayByName();
					break;
				case 3:
					displayMaximum();
					break;
				case 4:
					displayMinimum();
					break;
				case 5:
					displayBetween();
					break;
				case 6:
					addProduct();
					break;
				case 7:
					setPrice();
					break;
				case 8:
					setSoldYTD();
					break;
				case 9:
					print(products, "Display All");
					break;
				case 10:
					JOptionPane.showMessageDialog(inventoryManager,
							"Thank you for using the Inventory Manager.",
							"Exit", JOptionPane.PLAIN_MESSAGE);
					System.exit(0);
				default:
					JOptionPane.showMessageDialog(inventoryManager, "Invalid selection.",
							"Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(inventoryManager, "Invalid selection.",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public static void addProduct() {
		int productID = 0;
		String name = null;
		String description = null;
		String size = null;
		double price = 0;
		int soldYTD = 0;

		try {
			productID = Math.abs(Integer.parseInt(JOptionPane
					.showInputDialog("Please enter the product ID:")));
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid product ID entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (productID > 999) {
			JOptionPane.showMessageDialog(null,
					"Product ID entered is too high.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		name = JOptionPane.showInputDialog("Please enter the product name:");
		description = JOptionPane
				.showInputDialog("Please enter the product description:");
		size = JOptionPane.showInputDialog("Please enter the product size:");

		try {
			price = Double
					.parseDouble((JOptionPane
							.showInputDialog("Please enter the price of the product:")));
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid price entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (price > 99999.99) {
			JOptionPane.showMessageDialog(null, "Price entered is too high.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		try {
			soldYTD = Integer.parseInt(JOptionPane
					.showInputDialog("Please enter the quantity sold:"));
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid quantity entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		products.add(new Product(productID, name, size, price, soldYTD,
				description));
		sortProducts();
	}
	
	public static void createAndShowGUI(String frameName) {
		inventoryManager = new JFrame(frameName);
		inventoryManager.setDefaultCloseOperation(EXIT_ON_CLOSE);
		inventoryManager.setSize(800, 600);
		
		displayByIDButton = new JButton("Display product by ID");
		
		inventoryManager.getContentPane().add(displayByIDButton);
		inventoryManager.setVisible(true);
	}
	
	public static void displayBetween() {
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			double minPrice = Double.parseDouble(JOptionPane
					.showInputDialog("Please enter a minimum price:"));

			double maxPrice = Double.parseDouble(JOptionPane
					.showInputDialog("Please enter a maximum price:"));

			for (int i = 0; i < products.size(); i++) {
				Product current = products.get(i);
				try {
					if (current.getPrice() >= minPrice
							&& current.getPrice() <= maxPrice) {
						foundProducts.add(current);
					}
				} catch (NullPointerException e) {
				}
			}
			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Display between",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			print(foundProducts, "Display between");
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid price entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	public static void displayByID() {
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			int productID = Integer.parseInt(JOptionPane
					.showInputDialog("Please enter a valid product ID:"));

			for (int i = 0; i < products.size(); i++) {
				try {
					if (products.get(i).getProductID() == productID) {
						foundProducts.add(products.get(i));
					}
				} catch (NullPointerException e) {
				}
			}
			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Display by ID",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			print(foundProducts, "Display by ID");
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid product ID entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	public static void displayByName() {
		ArrayList<Product> foundProducts = new ArrayList<Product>();

		String name = JOptionPane.showInputDialog(
				"Please enter a product name:").toLowerCase();

		for (int i = 0; i < products.size(); i++) {
			try {
				if (products.get(i).getName().toLowerCase()
						.contains(name.toLowerCase())) {
					foundProducts.add(products.get(i));
				}
			} catch (NullPointerException e) {
			}
		}
		if (foundProducts.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"No matching product was found.", "Display by name",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		print(foundProducts, "Display by name");
	}

	public static void displayMaximum() {
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			double maxPrice = Double.parseDouble(JOptionPane
					.showInputDialog("Please enter a maximum price:"));

			for (int i = 0; i < products.size(); i++) {
				try {
					if (products.get(i).getPrice() <= maxPrice) {
						foundProducts.add(products.get(i));
					}
				} catch (NullPointerException e) {
				}
			}
			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Display maximum",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			print(foundProducts, "Display maximum");
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid price entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

	public static void displayMinimum() {
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			double minPrice = Double.parseDouble(JOptionPane
					.showInputDialog("Please enter a minimum price:"));

			for (int i = 0; i < products.size(); i++) {
				try {
					if (products.get(i).getPrice() >= minPrice) {
						foundProducts.add(products.get(i));
					}
				} catch (NullPointerException e) {
				}
			}
			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Display minimum",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			print(foundProducts, "Display minimum");
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid price entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	public static void print(ArrayList<Product> foundProducts,
			String windowTitle) {
		String output = String.format(
				"%-10s | %-20s | %-25s | %-8s | %-9s | %-8s\n", "Product ID",
				"Name", "Description", "Size", "Price", "Number Sold");

		for (int i = 0; i < 98; i++) {
			output += '-';
		}

		for (int i = 0; i < foundProducts.size(); i++) {
			try {
				output += "\n" + foundProducts.get(i).toString();
			} catch (NullPointerException e) {
			}
		}

		JOptionPane.showMessageDialog(null, output, windowTitle,
				JOptionPane.PLAIN_MESSAGE);
	}

	public static void setPrice() {
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			int productID = Integer.parseInt(JOptionPane
					.showInputDialog("Please enter the product ID:"));

			double price = Double.parseDouble((JOptionPane
					.showInputDialog("Please enter the new price:")));

			if (price > 99999.99) {
				JOptionPane.showMessageDialog(null,
						"Price entered is too high.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			for (int i = 0; i < products.size(); i++) {
				try {
					if (products.get(i).getProductID() == productID) {
						products.get(i).setPrice(price);
						foundProducts.add(products.get(i));
					}
				} catch (NullPointerException e) {
				}
			}

			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			String output = String.format("%-10s | %-20s | %-8s", "Product ID",
					"Name", "Number Sold");
			for (Product product : foundProducts) {
				output += String.format("\n%-10d | %-20s | $%-8.2f",
						product.getProductID(), product.getName(),
						product.getPrice());
			}
			JOptionPane.showMessageDialog(null, output, "Set price",
					JOptionPane.PLAIN_MESSAGE);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid product ID entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

	public static void setSoldYTD() {
		String output = String.format("%-10s | %-20s | %-8s", "Product ID",
				"Name", "Number Sold");
		try {
			ArrayList<Product> foundProducts = new ArrayList<Product>();
			int productID = Integer.parseInt(JOptionPane
					.showInputDialog("Please enter the product ID:"));

			int soldYTD = Integer.parseInt(JOptionPane
					.showInputDialog("Please enter the quantity sold:"));

			for (int i = 0; i < products.size(); i++) {
				try {
					if (products.get(i).getProductID() == productID) {
						products.get(i).setSoldYTD(soldYTD);
						foundProducts.add(products.get(i));
					}
				} catch (NullPointerException e) {
				}
			}

			if (foundProducts.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"No matching product was found.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			for (Product product : foundProducts) {
				output += String.format("\n%-10d | %-20s | %-8d",
						product.getProductID(), product.getName(),
						product.getSoldYTD());
			}
			JOptionPane.showMessageDialog(null, output, "Set number sold",
					JOptionPane.PLAIN_MESSAGE);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Invalid product ID entered.",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	public static void sortProducts() {
		boolean repeat = false;
		Product temp;

		do {
			repeat = false;
			for (int i = 1; i < products.size(); i++) {
				try {
					if (products.get(i - 1).compareTo(products.get(i)) < 0) {
						temp = new Product(products.get(i - 1));
						products.set(i - 1, new Product(products.get(i)));
						products.set(i, temp);
						repeat = true;
					}
				} catch (NullPointerException e) {
				}
			}
		} while (repeat);
	}
}