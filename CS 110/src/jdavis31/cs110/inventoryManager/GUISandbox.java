package jdavis31.cs110.inventoryManager;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;

import java.awt.CardLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Color;

import javax.swing.JButton;

import net.miginfocom.swing.MigLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUISandbox extends JFrame implements ItemListener {

	private final String ADD_PRODUCT = "Add product";
	private final String DISPLAY_BY_PRICE = "Display products by price";
	private final String DISPLAY_BY_ID = "Display products by ID";
	private final String DISPLAY_BY_NAME = "Display by name";
	private final String SET_PRICE = "Set product price";
	private final String SET_SOLD_YTD = "Set product sales";
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel cards;
	private JTextField idSearchField;
	private JTextField minPriceField;
	private JTextField maxPriceField;
	private JTextField addIDField;
	private JTextField addNameField;
	private JTextField addDescriptionField;
	private JTextField addSizeField;
	private JTextField addPriceField;
	private JTextField addYTDSalesField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
					GUISandbox frame = new GUISandbox();
					frame.pack();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUISandbox() {
		setTitle("GUI Sandbox");
		setBackground(new Color(240, 240, 240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		@SuppressWarnings({ "unchecked", "rawtypes" })
		JComboBox comboBox = new JComboBox(new String[] { ADD_PRODUCT,
				DISPLAY_BY_PRICE, DISPLAY_BY_ID, DISPLAY_BY_NAME, SET_PRICE,
				SET_SOLD_YTD });
		comboBox.addItemListener(this);
		contentPane.add(comboBox, BorderLayout.NORTH);

		cards = new JPanel();
		contentPane.add(cards, BorderLayout.CENTER);
		cards.setLayout(new CardLayout(0, 0));

		JPanel addProductPanel = new JPanel();
		cards.add(addProductPanel, ADD_PRODUCT);
		addProductPanel.setLayout(new MigLayout("", "[][336px,grow]",
				"[][][][][][][]"));

		JLabel addIDLabel = new JLabel("Product ID");
		addProductPanel.add(addIDLabel, "cell 0 0,alignx trailing");

		addIDField = new JTextField();
		addProductPanel.add(addIDField, "cell 1 0,growx");
		addIDField.setColumns(10);

		JLabel addNameLabel = new JLabel("Name");
		addProductPanel.add(addNameLabel,
				"cell 0 1,alignx trailing,aligny baseline");

		addNameField = new JTextField();
		addProductPanel.add(addNameField, "cell 1 1,growx");
		addNameField.setColumns(10);

		JLabel addDescriptionLabel = new JLabel("Description");
		addProductPanel.add(addDescriptionLabel, "cell 0 2,alignx trailing");

		addDescriptionField = new JTextField();
		addProductPanel.add(addDescriptionField, "cell 1 2,growx");
		addDescriptionField.setColumns(10);

		JLabel addSizeLabel = new JLabel("Size");
		addProductPanel.add(addSizeLabel, "cell 0 3,alignx trailing");

		addSizeField = new JTextField();
		addProductPanel.add(addSizeField, "cell 1 3,growx");
		addSizeField.setColumns(10);

		JLabel addPriceLabel = new JLabel("Price");
		addProductPanel.add(addPriceLabel, "cell 0 4,alignx trailing");

		addPriceField = new JTextField();
		addProductPanel.add(addPriceField, "cell 1 4,growx");
		addPriceField.setColumns(10);

		JLabel addYTDSalesLabel = new JLabel("YTD Sales");
		addProductPanel.add(addYTDSalesLabel, "cell 0 5,alignx trailing");

		addYTDSalesField = new JTextField();
		addProductPanel.add(addYTDSalesField, "cell 1 5,growx");
		addYTDSalesField.setColumns(10);

		JButton addProductButton = new JButton("Add product");
		addProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(
						contentPane,
						"Add product button pressed.\n{ "
								+ addIDField.getText() + ", "
								+ addNameField.getText() + ", "
								+ addDescriptionField.getText() + ", "
								+ addSizeField.getText() + ", "
								+ addPriceField.getText() + ", "
								+ addYTDSalesField.getText() + " }",
						"Add product", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		addProductPanel.add(addProductButton,
				"cell 1 6,alignx center,aligny center");

		JPanel displayByPricePanel = new JPanel();
		cards.add(displayByPricePanel, DISPLAY_BY_PRICE);
		displayByPricePanel.setLayout(new MigLayout("", "[][grow]", "[][][]"));

		JLabel minPriceLabel = new JLabel("Minimum Price");
		displayByPricePanel.add(minPriceLabel, "cell 0 0,alignx trailing");

		minPriceField = new JTextField();
		displayByPricePanel.add(minPriceField, "cell 1 0,growx");
		minPriceField.setColumns(10);

		JLabel maxPriceLabel = new JLabel("Maximum Price");
		displayByPricePanel.add(maxPriceLabel, "cell 0 1,alignx trailing");

		maxPriceField = new JTextField();
		displayByPricePanel.add(maxPriceField, "cell 1 1,growx");
		maxPriceField.setColumns(10);

		JButton priceSearchButton = new JButton("Search");
		priceSearchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(
						contentPane,
						"Price search button pressed.\n{ "
								+ minPriceField.getText() + ", "
								+ maxPriceField.getText() + " }",
						"Price Search", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		displayByPricePanel.add(priceSearchButton,
				"cell 1 2,alignx center,aligny center");

		JPanel displayByIDPanel = new JPanel();
		cards.add(displayByIDPanel, DISPLAY_BY_ID);
		displayByIDPanel.setLayout(new MigLayout("", "[][grow]", "[][]"));

		JLabel idSearchLabel = new JLabel("ID");
		idSearchLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		displayByIDPanel.add(idSearchLabel, "cell 0 0,alignx trailing");

		idSearchField = new JTextField();
		displayByIDPanel.add(idSearchField, "cell 1 0,growx");
		idSearchField.setColumns(10);

		JButton idSearchButton = new JButton("Search");
		idSearchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(
						contentPane,
						"Product ID search button pressed.\n{ "
								+ idSearchField.getText() + " }",
						"Product ID Search", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		displayByIDPanel.add(idSearchButton,
				"cell 1 1,alignx center,aligny center");

		JPanel displayByNamePanel = new JPanel();
		cards.add(displayByNamePanel, DISPLAY_BY_NAME);

		JPanel setPricePanel = new JPanel();
		cards.add(setPricePanel, SET_PRICE);

		JPanel setSoldYTDPanel = new JPanel();
		cards.add(setSoldYTDPanel, SET_SOLD_YTD);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		CardLayout cl = (CardLayout) (cards.getLayout());
		cl.show(cards, (String) e.getItem());
	}
}