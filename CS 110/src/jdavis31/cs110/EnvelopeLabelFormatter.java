package jdavis31.cs110;
import java.util.Scanner;

public class EnvelopeLabelFormatter {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		String fullName = null;
		String streetName = null;
		String houseNumber = null;
		String apartmentNumber = null;
		String city = null;
		String stateAbbreviation = null;
		String zipCode = null;
		
		System.out.println("Please enter your full name");
		fullName = input.nextLine();
		System.out.println("Please enter your house number");
		houseNumber = input.nextLine();
		System.out.println("Please enter your street name");
		streetName = input.nextLine();
		System.out.println("Please enter your apartment number");
		apartmentNumber = input.nextLine();
		System.out.println("Please enter your city");
		city = input.nextLine();
		System.out.println("Please enter your state abbreviation");
		stateAbbreviation = input.nextLine();
		System.out.println("Please enter your zip code");
		zipCode = input.nextLine();
		
		System.out.println(fullName);
		System.out.println(houseNumber + " " + streetName + ", Apt " + apartmentNumber);
		System.out.println(city + ", " + stateAbbreviation + " " + zipCode);
		input.close();
	}
}
