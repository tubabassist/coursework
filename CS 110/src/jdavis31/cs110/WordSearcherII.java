package jdavis31.cs110;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearcherII {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		Scanner inputFile = null;
		String filename = null;
		String word = null;
		File file = null;
		String line = null;
		int nrWordsFound = 0;
		
		System.out.println("Please enter the filename.");
		filename = input.nextLine();
		System.out.println("Please enter a word to search for.");
		word = input.nextLine();
		
		file = new File(filename);
		inputFile = new Scanner(file);
		
		while (inputFile.hasNextLine()) {
			line = inputFile.nextLine();
			for (int i = 0; i < line.length(); i++) {
				if (line.startsWith(word, i)) {
					nrWordsFound ++;
				}
			}
		}
		System.out.println("'" + word + "' was found " + nrWordsFound + " times.");
		input.close();
		inputFile.close();
	}
}