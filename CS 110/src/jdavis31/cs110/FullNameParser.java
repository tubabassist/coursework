package jdavis31.cs110;
import java.util.Scanner;

public class FullNameParser {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String firstName = null;
		String middleName = null;
		String lastName = null;
		String fullName = null;
		char firstInitial = 0;
		char secondInitial = 0;
		int posWhitespace = 0;
		int posWhitespace2 = 0;
		int age = 0;

		System.out.println("What is your age?");
		age = input.nextInt();
		input.nextLine();

		System.out.println((age >= 18) ? "OK" : "Denied");

		System.out.println("Please enter your full name");
		fullName = input.nextLine();

		posWhitespace = fullName.indexOf(' ');
		posWhitespace2 = fullName.lastIndexOf(' ');

		firstName = fullName.substring(0, posWhitespace);
		middleName = fullName.substring(posWhitespace + 1, posWhitespace2);
		lastName = fullName.substring(posWhitespace2 + 1);

		firstInitial = firstName.charAt(0);
		secondInitial = lastName.charAt(0);

		System.out.println(firstInitial + "." + secondInitial + ".");
		System.out.println(lastName + ", " + firstName + " " + middleName);

		input.close();
	}
}
