package jdavis31.cs110;

import java.util.Scanner;

public class LoginCredentialsChecker {

	public static void main(String[] args) {

		final String USERNAME = "NikolaJanevski";
		final String PASSWORD = "CS110_Fall2013";

		Scanner input = new Scanner(System.in);
		String inputUsername = null;
		String inputPassword = null;

		System.out.println("Please enter your username");
		inputUsername = input.next();
		System.out.println("Please enter your password");
		inputPassword = input.next();

		if (inputUsername.equalsIgnoreCase(USERNAME)) {
			if (inputPassword.equals(PASSWORD)) {
				System.out.println("Access granted");
			} else {
				System.out.println("Wrong password");
			}
		} else {
			System.out.println("Wrong username");
		}
		input.close();
	}
}