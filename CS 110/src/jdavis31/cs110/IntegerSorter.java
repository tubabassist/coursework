package jdavis31.cs110;
import java.util.Scanner;
import java.lang.Math;

public class IntegerSorter {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		int number1 = 0;
		int number2 = 0;
		int number3 = 0;
		
		System.out.println("Please enter three integers separated by whitespace:");
		number1 = input.nextInt();
		number2 = input.nextInt();
		number3 = input.nextInt(); 
		
		System.out.println("Smallest: " + Math.min(number3,  Math.min(number1, number2)));
		System.out.println("Middle: " + Math.min(number3, Math.max(number1, number2)));
		System.out.println("Largest: " + Math.max(number3, Math.max(number1, number2)));
		input.close();
	}
}