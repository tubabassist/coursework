package jdavis31.cs110;
import java.util.Scanner;

public class EmailAddressChecker {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String email = null;
		
		System.out.println("Please enter an email address");
		email = input.nextLine();
		
		if ((email.contains("@")))
		{
			if ((email.contains(" ")))
			{
				System.out.println("Email address cannot contain any spaces");
			}
			else
			{
				System.out.println("Valid email");
			}
		}
		else
		{
			System.out.println("Email address must contain @");
		}
		input.close();
	}
}
