package jdavis31.cs110;
import java.util.Scanner;

public class FactorialSum {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double inputInteger = 0;
		double answer = 0;

		System.out.println("Please enter a positive integer.");
		inputInteger = input.nextDouble();
		
		for (double i = 1; i <= inputInteger; i++) {
			answer += 1 / (3 * i);
		}
		System.out.printf("%5.4f", answer);
		input.close();
	}
}