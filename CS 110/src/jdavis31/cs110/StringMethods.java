package jdavis31.cs110;

public class StringMethods {

	public static boolean isLetter(char character) {
		return (isLowerCase(character) || isUpperCase(character));
	}
	
	public static boolean isLowerCase(char character) {
		if (character >= 'a' && character <= 'z') {
			return true;
		}
		return false;
	}
	
	public static boolean isUpperCase(char character) {
		if (character >= 'A' && character <= 'Z') {
			return true;
		}
		return false;
	}
		
	public static boolean isWord(String word) {
		for (int i = 0; i < word.length(); i++) {
			if (!Character.isLetter(word.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	public static String toLowerCase(String line) {
		String lowercase = "";
		
		for (int i = 0; i < line.length(); i ++) {
			if (isUpperCase(line.charAt(i))) {
				lowercase += (char)(line.charAt(i) - 'A' + 'a');
			}
			else {
				lowercase += line.charAt(i);
			}
		}
		return lowercase;
	}
}
