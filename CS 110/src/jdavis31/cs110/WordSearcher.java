package jdavis31.cs110;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class WordSearcher {
	public static void main(String[] args) throws FileNotFoundException {
		File book = null;
		Scanner inputFile = null;
		Scanner input = new Scanner(System.in);
		String filename = null;
		String line = null;
		String word = null;
		int nrLines = 0;
		
		System.out.println("Please enter the filename.");
		filename = input.nextLine();
		System.out.println("Please enter a word to search for.");
		word = input.nextLine();
		
		book = new File(filename);
		inputFile = new Scanner(book);
		
		while (inputFile.hasNextLine()) {
			line = inputFile.nextLine();
			if (line.contains(word)) {
				nrLines ++;
				System.out.println(line);
			}
		}
		System.out.println("There are " + nrLines + " lines containing '" + word + "'.");
		input.close();
		inputFile.close();
	}
}
