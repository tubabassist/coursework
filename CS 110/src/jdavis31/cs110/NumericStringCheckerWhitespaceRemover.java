package jdavis31.cs110;
import java.util.Scanner;

public class NumericStringCheckerWhitespaceRemover {

	public static boolean isNumeric(String word) {
		for (int i = 0; i < word.length(); i++) {
			if (!Character.isDigit(word.charAt(i))) {
				return false;
			}
		}
		return true;	
	}

	public static String removeWhitespaces(String text) {
		String noWhitespace = "";
		
		for (int i = 0; i < text.length(); i ++) {
			if (Character.isLetterOrDigit(text.charAt(i))) {
				noWhitespace += text.charAt(i);
			}
		}
		return noWhitespace;
	}
	
	//Main method to test my other methods.
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userInput = null;
		
		System.out.println("Please enter a string.");
		userInput = input.nextLine();
		
		System.out.println("Every character in the string is numeric: " + isNumeric(userInput));
		System.out.println(removeWhitespaces(userInput));
		input.close();
	}
}
