package jdavis31.cs110;
import java.util.Arrays;

public class SortingMethods {
	public static int[] BubbleSort(int[] array) {
		boolean repeat = true;
		int temp = 0;
		
		while (repeat) {
			repeat = false;
			for (int i = 1; i < array.length; i++) {
				if (array[i] < array[i - 1]) {
					temp = array[i - 1];
					array[i - 1] = array[i];
					array[i] = temp;
					repeat = true;
				}
			}
		}
		
		return array;
	}
	
	public static int[] InsertionSort(int[] array) {
		int temp = 0;
		
		for (int i = 2; i < array.length; i++) {
			for (int j = i; j > 1 && array[j] < array[j - 1]; j--) {
				temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
			}
		}
		
		return array;
	}
	
	public static int[] SelectionSort(int[] array) {
		int minPos = 0;
		int temp = 0;
		
		for (int i = 1; i < array.length; i++) {
			minPos = i;
			for (int j = i+1; j < array.length; j++) {
				if (array[j] < array[minPos]) minPos = j;
			}
			temp = array[i];
			array[i] = array[minPos];
			array[minPos] = temp;
		}
		
		return array;
	}
	
	public static void main(String[] args) {
		int[] array = new int[(int)(Math.random() * 20)];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = (int)(Math.random() * 100);
		}
		
		System.out.println(Arrays.toString(array));
		System.out.println(Arrays.toString(BubbleSort(array)));
		System.out.println(Arrays.toString(SelectionSort(array)));
		System.out.println(Arrays.toString(InsertionSort(array)));
	}
}