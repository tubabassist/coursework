package jdavis31.cs110;
public class CalculationOnXLooped {
	public static void main(String[] args) {
		for (double x = 0; x <= 10; x += 0.5) {
			System.out.printf("%10.2f %10.2f %10.2f %10.2f\n", x, Math.sqrt(x),
					Math.pow(x, 2), Math.pow(x, 3));
		}
	}
}
