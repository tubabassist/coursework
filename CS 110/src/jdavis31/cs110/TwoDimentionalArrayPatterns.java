package jdavis31.cs110;

public class TwoDimentionalArrayPatterns {
	public static void main(String[] args) {
		char[][] alpha = new char[5][10];
		
		for (int i = 0; i < alpha.length; i++) {
			java.util.Arrays.fill(alpha[i], 'B');
		}
			
		for (int i = 0; i < alpha.length; i++) {
			String array = java.util.Arrays.toString(alpha[i]);
			System.out.println(array);
		}
		
		for (int i = 0; i < alpha.length; i++) {
			for (int j = 0; j < alpha[i].length; j++) {
				if (i == 0 || i == alpha.length - 1 || j == 0 || j == alpha[i].length - 1) {
					alpha[i][j] = 'A';
				}
				else {
					alpha[i][j] = 'Z';
				}
			}
		}
		
		System.out.println();
		for (int i = 0; i < alpha.length; i++) {
			String array = java.util.Arrays.toString(alpha[i]);
			System.out.println(array);
		}
		
		int [][] beta = new int[3][3];
		int i, j;

		for ( i = 0; i < 3; i++) {
			for (j=0; j < 3; j++) {
				beta[j][i] = i * (j * 2);
			}
		}
		
		System.out.println();
		for (int j1 = 0; j1 < beta.length; j1++) {
			String array = java.util.Arrays.toString(beta[j1]);
			System.out.println(array);
		}
	}
}