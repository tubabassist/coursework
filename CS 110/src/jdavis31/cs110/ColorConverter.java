package jdavis31.cs110;
import java.util.Scanner;

public class ColorConverter {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		double red = 0;
		double green = 0;
		double blue = 0;
		final double MAX_SCALE = 255;
		double cyan = 0;
		double magenta = 0;
		double yellow = 0;
		double black = 0;
		double w = 0;
		
		System.out.println("Please enter the value for red");
		red = input.nextInt();
		System.out.println("Please enter the value for green");
		green = input.nextInt();
		System.out.println("Please enter the value for blue");
		blue = input.nextInt();
		
		w = Math.max(red / MAX_SCALE, Math.max(green / MAX_SCALE, blue / MAX_SCALE));
		cyan = (w - (red / MAX_SCALE)) / w;
		magenta = (w-(green / MAX_SCALE)) / w;
		yellow = (w- (blue / MAX_SCALE)) / w;
		black = 1 - w;
		
		System.out.println("Cyan: " + cyan);
		System.out.println("Magenta: " + magenta);
		System.out.println("Yellow: " + yellow);
		System.out.println("Black: " + black);
		input.close();
	}
}
