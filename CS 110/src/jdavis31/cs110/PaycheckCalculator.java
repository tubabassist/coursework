package jdavis31.cs110;
import javax.swing.JOptionPane;

public class PaycheckCalculator {
	public static void main(String [] args) {
		
		final double FEDERAL_INCOME_TAX_RATE = 0.15;
		final double STATE_TAX_RATE = 0.035;
		final double SOCIAL_SECURITY_TAX_RATE = 0.0575;
		final double MEDICARE_MEDICAID_TAX_RATE = 0.0275;
		final double PENSION_PLAN_RATE = 0.05;
		final double HEALTH_INSURANCE = 75;
		
		String employeeName = null;
		String inputIncome = null;
		double grossPay = 0;
		double federalIncomeTaxDeduction = 0;
		double stateTaxDeduction = 0;
		double socialSecurityTaxDeduction = 0;
		double medicareMedicaidTaxDeduction = 0;
		double pensionPlanDeduction = 0;
		double netPay = 0;
		
		employeeName = JOptionPane.showInputDialog("Please enter the name of the employee");
		inputIncome = JOptionPane.showInputDialog("Please enter the gross pay of the employee");
		grossPay = Double.parseDouble(inputIncome);
		
		federalIncomeTaxDeduction = grossPay * FEDERAL_INCOME_TAX_RATE;
		stateTaxDeduction = grossPay * STATE_TAX_RATE;
		socialSecurityTaxDeduction = grossPay * SOCIAL_SECURITY_TAX_RATE;
		medicareMedicaidTaxDeduction = grossPay * MEDICARE_MEDICAID_TAX_RATE;
		pensionPlanDeduction = grossPay * PENSION_PLAN_RATE;
		netPay = grossPay - federalIncomeTaxDeduction - stateTaxDeduction - socialSecurityTaxDeduction - medicareMedicaidTaxDeduction
				- pensionPlanDeduction - HEALTH_INSURANCE;
		
		System.out.printf("%s \n%-26s$ %7.2f\n", employeeName, "Gross Amount:", grossPay);
		System.out.printf("%-26s$ %7.2f\n", "Federal Tax:", federalIncomeTaxDeduction);
		System.out.printf("%-26s$ %7.2f\n", "State Tax:", stateTaxDeduction);
		System.out.printf("%-26s$ %7.2f\n", "Social Security Tax:", socialSecurityTaxDeduction);
		System.out.printf("%-26s$ %7.2f\n", "Medicare/Medicaid Tax:", medicareMedicaidTaxDeduction);
		System.out.printf("%-26s$ %7.2f\n", "Pension Plan:", pensionPlanDeduction);
		System.out.printf("%-26s$ %7.2f\n", "Health Insurance:", HEALTH_INSURANCE);
		System.out.printf("%-26s$ %7.2f\n", "Net Pay:", netPay);
	}
}