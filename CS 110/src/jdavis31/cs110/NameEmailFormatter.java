package jdavis31.cs110;
import java.util.Scanner;

public class NameEmailFormatter {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		String fullName = null;
		String email = null;
		
		System.out.println("Please enter your full name");
		fullName = input.nextLine();
		System.out.println("Please enter your email address");
		email = input.nextLine();
		
		System.out.println('"' + fullName + '"' + " <" + email + '>');
		input.close();
	}
}
