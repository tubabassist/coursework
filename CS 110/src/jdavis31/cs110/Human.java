package jdavis31.cs110;

public class Human {
	private String name;
	private int age;
	private int weight;
	private double height;
	
	public void print() {
		System.out.println("Name: " + name);
		System.out.println("Age: " + age);
		System.out.println("Weight: " + weight);
		System.out.println("Height: " + height);
	}
	
	public void setAge(int newAge) {
		age = newAge;
	}
	
	public void setWeight(int newWeight) {
		weight = newWeight;
	}
	
	public void setHeight(double newHeight) {
		height = newHeight;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
}