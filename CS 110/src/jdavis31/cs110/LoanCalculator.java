package jdavis31.cs110;
import java.util.Scanner;
import java.lang.Math;

public class LoanCalculator {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		double principal = 0;
		double interestRate = 0;
		double years = 0;
		
		System.out.println("Please enter the principal");
		principal = input.nextDouble();
		System.out.println("Please enter the interest rate");
		interestRate = input.nextDouble();
		System.out.println("Please enter the number of years");
		years = input.nextDouble();
		
		System.out.println("The total value of the loan is " + (principal * Math.exp(interestRate * years)));
		input.close();
	}
}
