package jdavis31.cs110;

public class ArrayMethods {
	public static void generateRandom(double[] numbers, double maxRange) {
		System.out.println("Inside random for double.");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = Math.random() * maxRange;
		}
	}
	
	public static void generateRandom(int[] numbers, int maxRange) {
		System.out.println("Inside random for int.");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = (int)(Math.random() * maxRange);
		}
	}
	
	public static double meanOfArray(double[] array) {
		return sumArray(array) / array.length;
	}
	
	public static int meanOfArray(int[] array) {
		return sumArray(array) / array.length;
	}
	
	public static void printArray(double[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			System.out.printf("%-10.2f ", numbers[i]);
		}
		
		System.out.println();
	}
	
	public static void printArray(int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
		
		System.out.println();
	}
	
	public static int[] squareArray(int[] array) {
		int[] squares = new int[array.length];
		
		for (int i = 0; i < array.length; i++) {
			squares[i] = array[i] * array[i];
		}
		
		return squares;
	}
	
	public static double sumArray(double[] array) {
		double sum = 0;
		
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum;
	}
	
	public static int sumArray(int[] array) {
		int sum = 0;
		
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum;
	}
	
	public static void main(String[] args) {
		final int NR_ELEMENTS = 10;
		int[] array = new int[NR_ELEMENTS];
		int[] squares = null;
		double[] decimal = new double[NR_ELEMENTS];
		
		generateRandom(array, 100);
		printArray(array);
		squares = squareArray(array);
		printArray(squares);
		System.out.println(sumArray(array));
		System.out.println(meanOfArray(array));
		
		generateRandom(decimal, 100);
		printArray(decimal);
		System.out.println(sumArray(decimal));
		System.out.println(meanOfArray(decimal));
	}
}
