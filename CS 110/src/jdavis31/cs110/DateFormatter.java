package jdavis31.cs110;
public class DateFormatter {
	public static void main(String [] args) {
		int day = 17;
		int month = 1;
		int year = 2013;
		
		System.out.println(month + " " + day + " " + year);
		System.out.printf("%02d-%02d-%d\n", month, day, year);
		System.out.printf("%-9s %04d\n", "Month:", month);
		System.out.printf("%-9s %04d\n", "Day:", day);
		System.out.printf("%-9s %04d\n", "Year:", year);
		System.out.println("------------------------------");
		System.out.printf("|%8s |%8s |%8s|\n", "Month", "Day", "Year");
		System.out.println("------------------------------");
		System.out.printf("|%8d |%8d |%8d|\n", month, day, year);
		System.out.println("------------------------------");
	}
}
