package jdavis31.cs110;
import java.util.Scanner;

public class WhitespaceCounter {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userInput = null;
		int numberOfWhitespaces = 0;
	
		System.out.println("Please enter a sentence.");
		userInput = input.nextLine();
	
		for (int i = 0; i < userInput.length(); i++) {
			if (userInput.charAt(i) == ' ') {
				numberOfWhitespaces++;
			}
		}
		System.out.println(numberOfWhitespaces);
		input.close();
	}
}
