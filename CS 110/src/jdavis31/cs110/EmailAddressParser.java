package jdavis31.cs110;
import javax.swing.JOptionPane;

public class EmailAddressParser {
	public static void main(String[] args) {
		String input = null;

		input = JOptionPane.showInputDialog("Please enter your email address");

		if (input.indexOf('@') <= -1) {
			JOptionPane.showMessageDialog(null,
					"The email address you entered is invalid.", "Error",
					JOptionPane.ERROR_MESSAGE);
		}

		else {
			JOptionPane
					.showMessageDialog(
							null,
							String.format(
									"The email address entered was '%s'.  The \nusername is '%s' and the domain name is '%s'.",
									input,
									input.substring(0, input.indexOf('@')),
									input.substring(input.indexOf('@') + 1)),
							"Email Address Parser",
							JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
