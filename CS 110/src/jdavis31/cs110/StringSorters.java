package jdavis31.cs110;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StringSorters {
	public static void insertionSort(String[] words) {
		String firstWord = null;
		String currentWord = null;
		
		System.out.printf("\nInsertion sorting:\n");
		
		for (int i = 0; i < words.length; i++) {
			firstWord = words[i];
			
			for (int j = i - 1; j >= 0; j--) {
				currentWord = words[j];
					if (firstWord.compareTo(currentWord) < 0) {
						words[j + 1] = currentWord;
					}
				words[j] = firstWord;
			}

			System.out.println(words[i]);
		}
	}
	
	public static void selectionSort(String[] words) {
		String firstWord = null;
		String currentWord = null;
		int minPos = 0;
		
		System.out.printf("\nSelection sorting:\n");
		
		for (int i = 0; i < words.length; i++) {
			firstWord = words[i];
			
			for (int j = i + 1; j < words.length; j++) {
				currentWord = words[j];
				if (firstWord.compareTo(currentWord) > 0) {
					firstWord = words[j];
					minPos = j;
				}
			}
			
			if (!firstWord.equals(words[i])) {
				words[minPos] = words[i];
				words[i] = firstWord;
			}
			System.out.println(words[i]);
		}
	}
	
	public static void main(String [] args) {
		Scanner userInput = new Scanner(System.in);
		String filename = null;
		Scanner inputFile = null;
		File file = null;
		boolean invalidInput = false;
		
		do {
			invalidInput = false;
			System.out.println("Please enter the filename.");
			filename = userInput.nextLine();
			file = new File(filename);
			try {
				inputFile = new Scanner(file);
			} catch (FileNotFoundException e) {
				System.out.println("File '" + filename + "' does not exist - please try again.");
				invalidInput = true;
			}
		}
		while (invalidInput);
		
		String[] words = new String[inputFile.nextInt()];
		inputFile.nextLine();
		
		for (int i = 0; i < words.length; i++) {
			words[i] = inputFile.nextLine();
		}
		
		selectionSort(words);
		insertionSort(words);
		userInput.close();
	}
}