package jdavis31.cs110;
import javax.swing.JOptionPane;

public class CalculationsOnX {
	public static void main(String[] args) {
		String userInput = null;
		double x = 0;
		String output = null;

		userInput = JOptionPane.showInputDialog("Please enter a value for x");
		x = Double.parseDouble(userInput);

		output = String.format("%10.2f %10.2f %10.2f %10.2f", x, Math.sqrt(x),
				Math.pow(x, 2), Math.pow(x, 3));
		JOptionPane.showMessageDialog(null, output, "Calculations on x",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
