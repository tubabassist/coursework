package jdavis31.cs110.vehicle;
public class Vehicle {

	private String model;
	private String make;
	private String owner;
	private double mpg;
	private double fuelLevel;
	private double odometer;
	private String engineType;
	private int modelYear;
	private int tankSize;

	public Vehicle(String mdl, String mke, String own, double milespergallon,
			double fuelLvl, double odo, String engType, int mdlYear) {

		tankSize = 25;
		model = mdl;
		make = mke;
		owner = own;
		mpg = milespergallon;
		fuelLevel = fuelLvl;
		odometer = odo;
		engineType = engType;
		modelYear = mdlYear;
	}

	public Vehicle() {
		model = null;
		make = null;
		owner = null;
		mpg = 0.0;
		fuelLevel = 0.0;
		odometer = 0.0;
		engineType = null;
		modelYear = 0;
		tankSize = 25;
	}

	public String toString() {
		String output = "The make of the vehicle is " + make + " and model is "
				+ model + " and the owner is " + owner + " and mpg is " + mpg
				+ " and the fuellevel is " + fuelLevel + " the odometer is "
				+ odometer + " and the engintype is " + engineType
				+ " and the model year is " + modelYear;

		return output;
	}

	public void addFuel(double amount) {
		if (amount < 0) {
			throw new RuntimeException("amount of fuel give must be >=0");
		} else {
			if ((fuelLevel + amount) > tankSize) {
				throw new RuntimeException(
						"You added too much gas to the tank BANG!");
			} else {
				fuelLevel = fuelLevel + amount;
			}
		}
	}

	public String getMake() {
		return this.make;
	}

	public void setMake(String newMake) {
		this.make = newMake;
	}

}