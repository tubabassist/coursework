package jdavis31.cs110.vehicle;

public class VehicleTester {
	public static void main(String[] args) {
		
		Vehicle myCar = new Vehicle("Explorer", "Ford", "Joshua Davis", 16.0,  15.0,
			150000, "V6", 1992);
		
		System.out.println("The make of the car is " + myCar.getMake());
		
		myCar.setMake("DMC");
		
		System.out.println("The make of the car is " + myCar.getMake());
	}
}
