package jdavis31.cs110;
import java.util.Scanner;

public class QuadraticFormula {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		double a = 0;
		double b = 0;
		double c = 0;
		double x1 = 0;
		double x2 = 0;
		
		System.out.println("Please enter the value of a");
		a = input.nextDouble();
		System.out.println("Please enter the value of b");
		b = input.nextDouble();
		System.out.println("Please enter the value of c");
		c = input.nextDouble();
		
		x1 = (- b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
		x2 = (- b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
		
		System.out.println("x = " + x2 + " or " + x1);
		input.close();
	}
}
