package jdavis31.cs110;
import javax.swing.JOptionPane;

public class AutoLoanCalculator {
	public static void main(String [] args) {
		String make = null;
		String model = null;
		String inputYear = null;
		int year = 0;
		String inputPrice = null;
		double price = 0;
		String inputDownPayment = null;
		double downPayment = 0;
		String inputMonths = null;
		int months = 0;
		double monthlyPayment = 0;
		String output = null;
		final double RATE = 0.069;
		
		inputYear = JOptionPane.showInputDialog("Please enter the year of the vehicle");
		year = Integer.parseInt(inputYear);
		make = JOptionPane.showInputDialog("Please enter the make of the vehicle");
		model = JOptionPane.showInputDialog("Please enter the model of the vehicle");
		inputPrice = JOptionPane.showInputDialog("Please enter the price of the vehicle");
		price = Double.parseDouble(inputPrice);
		inputDownPayment = JOptionPane.showInputDialog("Please enter the down payment on the vehicle");
		downPayment = Double.parseDouble(inputDownPayment);
		inputMonths = JOptionPane.showInputDialog("Would you like to finance the vehicle for 48, 60, or 72 months?");
		months = Integer.parseInt(inputMonths);
	
		switch (months) {
			case 48:
			case 60:
			case 72:
				monthlyPayment = ((price - downPayment) * (RATE / 12)) / (1 - Math.pow((1 + (RATE / 12)), -months));
				output = String.format("%d %s %s \n----------------------------------------------\nBase Price: $%6.2f \nDown Payment: $%6.2f"
						+ "\nFinanced for %2d months" + " at $%6.2f per month", year, make, model, price, downPayment, months, monthlyPayment);
				JOptionPane.showMessageDialog(null, output, "Calculations on x", JOptionPane.INFORMATION_MESSAGE);
				break;
			default:
				JOptionPane.showMessageDialog(null, "The finance period you entered is invalid.", "Error", JOptionPane.ERROR_MESSAGE);
				break;
		}
	}
}
