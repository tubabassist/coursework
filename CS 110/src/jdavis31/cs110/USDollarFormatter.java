package jdavis31.cs110;
import javax.swing.JOptionPane;

public class USDollarFormatter {
	public static void main(String[] args) {

		JOptionPane.showMessageDialog(null, String.format(
				"The price is $%.2f\n", Double.parseDouble(JOptionPane
						.showInputDialog("Please enter the price"))), "Price",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
