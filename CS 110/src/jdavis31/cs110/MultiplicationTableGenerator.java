package jdavis31.cs110;
import java.util.Scanner;

public class MultiplicationTableGenerator {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 0;
		
		System.out.println("Please enter a maximum:");
		number = input.nextInt();
		
		for (int i = 1; i <= number; i++) {
			for (int j = 1; j <= number; j++) {
				System.out.printf("%4d", i * j);
			}
			System.out.println();
		}
		input.close();
	}
}
