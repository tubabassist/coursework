package jdavis31.cs110;
import java.util.Scanner;

public class InchConverter {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double inches = 0;
		double convertedValue = 0;
		int selection = 0;
		final double INCHES_PER_FOOT = 12;
		final double INCHES_PER_METER = 39.3701;
		final double INCHES_PER_YARD = 36;

		System.out.println("Please enter a length in inches");
		inches = input.nextDouble();
		System.out.println("To which metric would you like to convert?\n"
				+ "Enter one of the following options:\n" + "1) Feet\n"
				+ "2) Meters\n" + "3) Yards\n");
		selection = input.nextInt();

		switch (selection) {
		case 1:
			convertedValue = inches / INCHES_PER_FOOT;
			if (Math.abs(convertedValue) == 1) {
				System.out.println(convertedValue + " foot");
			} else {
				System.out.println(convertedValue + " feet");
			}
			break;
		case 2:
			convertedValue = inches / INCHES_PER_METER;
			if (Math.abs(convertedValue) == 1) {
				System.out.println(convertedValue + " meter");
			} else {
				System.out.println(convertedValue + " meters");
			}
			break;
		case 3:
			convertedValue = inches / INCHES_PER_YARD;
			if (Math.abs(convertedValue) == 1) {
				System.out.println(convertedValue + " yard");
			} else {
				System.out.println(convertedValue + " yards");
			}
			break;
		default:
			System.out.println("Invalid selection");
		}
		input.close();
	}
}