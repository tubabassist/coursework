package jdavis31.cs110;
import java.util.Scanner;

public class CoordinateConverter {
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		double x = 0;
		double y = 0;
		
		System.out.println("Please enter coordinates x and y separated by whitespace:");
		x = input.nextDouble();
		y = input.nextDouble();
		
		System.out.println("The radius is " + Math.sqrt(x*x + y*y));
		System.out.println("The angle is " + Math.atan2(y, x));
		System.out.println("The min is " + Math.min(x, y));
		input.close();
	}
}
