package jdavis31.cs110;

import java.util.Scanner;

public class FacebookURLParser {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String facebookURL = null;

		System.out
				.println("Please enter the full URL of your Facebook profile");
		facebookURL = input.next();

		System.out
				.println(facebookURL.substring(facebookURL.lastIndexOf('/') + 1));
		input.close();
	}
}
