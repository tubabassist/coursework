package jdavis31.cs110;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class CharacterCounter {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		Scanner inputFile = null;
		String filename = null;
		File file = null;
		String line = null;
		int letters = 0;
		int blankSpaces = 0;
		int digits = 0;
		int otherCharacters = 0;
		int words = 0;
		int numberOfLines = 0;

		System.out.println("Please enter the filename.");
		filename = input.nextLine();

		file = new File(filename);
		inputFile = new Scanner(file);

		while (inputFile.hasNextLine()) {
			line = inputFile.nextLine();
			for (int i = 0; i < line.length(); i++) {
				if (Character.isLetter(line.charAt(i))) {
					letters++;
				}
				if (Character.isWhitespace(line.charAt(i))) {
					blankSpaces++;
				}
				if (Character.isDigit(line.charAt(i))) {
					digits++;
				}
				if (!Character.isLetterOrDigit(line.charAt(i))
						&& line.charAt(i) != ' ') {
					otherCharacters++;
				}
				// Just in case there are two white spaces in a row.
				if (line.charAt(i) == ' ' && line.charAt(i - 1) != ' ') {
					words++;
				}
			}
			// Just in case the last character of a line is not a white space.
			if (line.charAt(line.length() - 1) != ' ') {
				words++;
			}
			numberOfLines++;
		}
		System.out
				.println("There are " + numberOfLines + " lines in the file.");
		System.out.println("There are " + words + " words in the file.");
		System.out.println("There are " + letters + " letters in the file.");
		System.out.println("There are " + blankSpaces
				+ " blank spaces in the file.");
		System.out.println("There are " + digits + " digits in the file.");
		System.out.println("There are " + otherCharacters
				+ " other characters in the file.");
		input.close();
		inputFile.close();
	}
}
