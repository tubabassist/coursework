package jdavis31.cs110;

public class BigNamePrinter {
	public static void main(String [] args) {
		System.out.println(" ****   ***    ****  *   *  *   *   ***");
		System.out.println("    *  *   *  *      *   *  *   *  *   *");
		System.out.println("    *  *   *   ***   *****  *   *  *****");
		System.out.println("*   *  *   *      *  *   *  *   *  *   *");
		System.out.println(" ***    ***   ****   *   *   ***   *   *");
	}
}