package jdavis31.cs110;
import java.util.Scanner;

public class StatusParser {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String status = null;
		int beginUsertag = 0;
		int endUsertag = 0;
		int beginHashtag = 0;
		int endHashtag = 0;
		int beginURL = 0;
		int endURL = 0;
		String hashtag = null;
		String usertag = null;
		String url = null;

		System.out
				.println("Please enter a Twitter/Facebook status that contains a @usertag, #hashtag, and http://URL:");
		status = input.nextLine();

		beginUsertag = status.indexOf('@');
		endUsertag = status.indexOf(' ', beginUsertag);
		beginHashtag = status.indexOf('#');
		endHashtag = status.indexOf(' ', beginHashtag);
		beginURL = status.indexOf("http://");
		endURL = status.indexOf(' ', beginURL);

		hashtag = (endHashtag > -1) ? status.substring(beginHashtag + 1,
				endHashtag) : status.substring(beginHashtag);
		usertag = (endUsertag > -1) ? status.substring(beginUsertag + 1,
				endUsertag) : status.substring(beginUsertag);
		url = (endURL > -1) ? status.substring(beginURL, endURL) : status
				.substring(beginURL);

		System.out.println("hashtag:	" + hashtag);
		System.out.println("usertag:	" + usertag);
		System.out.println("URL:		" + url);
		input.close();
	}
}
