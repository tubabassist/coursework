package jdavis31.cs110;
import java.util.Scanner;

public class BinaryConverter {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userInput = null;
		int decimal = 0;
		String hexadecimal = null;
		
		System.out.println("Please enter a binary number.");
		userInput = input.next();
		
		for (int i = 0; i < userInput.length(); i++) {
			if (userInput.charAt(userInput.length() - 1 - i) == '1') {
				decimal += 1 * Math.pow(2, i);
			}
		}
		
		int i = Integer.parseInt(userInput, 2);
		hexadecimal = Integer.toHexString(i);
		System.out.printf("%s %s\n%s 0x%s", "Decimal:", decimal, "Hexadecimal:", hexadecimal);
		input.close();
	}
}
