package jdavis31.cs110;
public class ExponentFunctions {

	public static double pow(double x, int exponent) {
		double power = 1;

		for (int i = 1; i <= exponent; i++) {
			power = power * x;
		}
		return power;
	}

	public static double sqr(double x) {
		return pow(x, 2);
	}

	public static void main(String[] args) {
		double square = 0;
		double cube = 0;

		for (double i = 1; i <= 10; i++) {
			square = ExponentFunctions.sqr(i);
			cube = ExponentFunctions.pow(i, 3);
			System.out.printf("x^2 = %8.2f x^3 = %8.2f\n", square, cube);
		}
	}
}
