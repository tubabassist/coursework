package jdavis31.cs110;
import java.util.Scanner;

public class DateParser {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String date = null;
		int posWhitespace = 0;
		int posComma = 0;
		String month = null;
		String day = null;
		String year = null;

		System.out
				.println("Please enter a date in the following format: month day, year");
		date = input.nextLine();

		posWhitespace = date.indexOf(' ');
		posComma = date.indexOf(',');
		month = date.substring(0, posWhitespace);
		day = date.substring(posWhitespace + 1, posComma);
		year = date.substring(posComma + 2);

		System.out.println(day + " - " + month + " - " + year);
		input.close();
	}
}