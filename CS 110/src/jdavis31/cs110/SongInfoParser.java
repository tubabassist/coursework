package jdavis31.cs110;
import java.util.Scanner;

public class SongInfoParser {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String playlistItem = null;
		int dash1 = 0;
		int dash2 = 0;
		String artistName = null;
		String albumName = null;
		String songTitle = null;

		System.out
				.println("Please enter a playlist item in the following format:");
		System.out.println("Artist name - Album name - Song title");
		playlistItem = input.nextLine();

		dash1 = playlistItem.indexOf('-');
		dash2 = playlistItem.indexOf('-', dash1 + 1);

		artistName = playlistItem.substring(0, dash1);
		albumName = playlistItem.substring(dash1 + 2, dash2);
		songTitle = playlistItem.substring(dash2 + 2);

		System.out.println("Artist: " + artistName);
		System.out.println("Album: " + albumName);
		System.out.println("Song: " + songTitle);

		input.close();
	}
}