package jdavis31.cs110;
import javax.swing.JOptionPane;

public class ByteConverter {
	public static void main(String[] args) {
		final double BYTES_IN_KILOBYTE = Math.pow(2, 10);
		final double BYTES_IN_MEGABYTE = Math.pow(2, 20);
		final double BYTES_IN_GIGABYTE = Math.pow(2, 30);
		final double BYTES_IN_TERABYTE = Math.pow(2, 40);
		String inputBytes = null;
		double bytes = 0;
		String output = null;

		inputBytes = JOptionPane
				.showInputDialog("Please enter a positive integer representing a number of bytes.");
		bytes = Long.parseLong(inputBytes);

		output = String
				.format("%.0f bytes \n%f kilobytes \n%f megabytes \n%f gigabytes \n%f terabytes",
						bytes, bytes / BYTES_IN_KILOBYTE, bytes
								/ BYTES_IN_MEGABYTE, bytes / BYTES_IN_GIGABYTE,
						bytes / BYTES_IN_TERABYTE);
		JOptionPane.showMessageDialog(null, output, "Byte Converter",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
