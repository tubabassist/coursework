package jdavis31.cs110;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class PunctuationCounter {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		Scanner inputFile = null;
		File file = null;
		String filename = null;
		String line = null;
		int nrPunctuation = 0;
		
		System.out.println("Please enter a filename.");
		filename = input.nextLine();
		
		file = new File(filename);
		inputFile = new Scanner(file);
		
		while (inputFile.hasNextLine()) {
			line = inputFile.nextLine();
			for (int i = 0; i < line.length(); i++) {
				if (!Character.isLetterOrDigit(line.charAt(i))) {
					nrPunctuation ++;
				}
			}
		}
		System.out.println("There are " + nrPunctuation + " punctuation characters.");
		input.close();
		inputFile.close();
	}
}
