package jdavis31.cs110;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TokenCounter {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		Scanner inputFile = null;
		File file = null;
		String token = null;
		boolean invalidInput = false;
		int wordCount = 0;
		int integerCount = 0;
		int floatCount = 0;

		do {
			invalidInput = false;
			//System.out.println("Please enter the filename.");
			//file = new File(userInput.nextLine());
			file = new File("testdata.txt");
			try {
				inputFile = new Scanner(file);
			} catch (FileNotFoundException e) {
				System.out.println("File does not exist.  Please try again.");
				invalidInput = true;
			}
		} while (invalidInput);

		while (inputFile.hasNext()) {
			token = inputFile.next();
			if (isWord(token)) {
				wordCount++;
			} else {
				if (isInteger(token)) {
					integerCount++;
				} else {
					if (isFloat(token)) {
						floatCount++;
					} else {
						System.out.println("Invalid token: " + token);
					}
				}
			}
		}

		System.out.println("There are " + wordCount + " words in this file.");
		System.out.println("There are " + integerCount
				+ " integers in this file.");
		System.out.println("There are " + floatCount
				+ " floating point numbers in this file.");
		userInput.close();
	}

	public static boolean isFloat(String token) {
		if (Character.isDigit(token.charAt(0)) || (token.charAt(0) == '+')
				|| (token.charAt(0) == '-') || (token.charAt(0) == '.')) {
			for (int i = 1; i < token.length(); i++) {
				if (!Character.isLetter(token.charAt(i))) {
					if ((token.charAt(i - 1) == '.')
							&& Character.isDigit(token.charAt(i)))
						return true;
				}
			}
		}
		return false;
	}

	public static boolean isInteger(String token) {
		if (Character.isDigit(token.charAt(0)) || (token.charAt(0) == '+')
				|| (token.charAt(0) == '-')) {
			for (int i = 1; i < token.length(); i++) {
				if (!Character.isDigit(token.charAt(i)))
					return false;
			}
		} else
			return false;
		return true;
	}

	public static boolean isWord(String token) {
		for (int i = 0; i < token.length(); i++) {
			if (!Character.isLetter(token.charAt(i)))
				return false;
		}
		return true;
	}
}
