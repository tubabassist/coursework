package jdavis31.cs110;
import java.util.Scanner;

public class DivisibilityChecker {
	
	public static boolean isDivisible(int number, int divisor) {
		if (number % divisor == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isEven(int number) {
		if (isDivisible(number, 2) == true) {
			return true;
		}
		return false;
	}
	
	//Main method to test my other methods.
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int integer = 0;
		int divisor = 0;
		
		System.out.println("Please enter an integer:");
		integer = input.nextInt();
		System.out.println("Please enter an integer divisor:");
		divisor = input.nextInt();
		
		System.out.println(integer + " is divisible by " + divisor + ": " + isDivisible(integer, divisor));
		System.out.println(integer + " is even: " + isEven(integer));
		input.close();
	}
}
