package jdavis31.cs110;
import javax.swing.JOptionPane;

public class SecConverter {
	public static void main(String[] args) {
		String userInput = null;
		int timeInSeconds = 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		String output = null;
		final int SEC_IN_HOUR = 3600;
		final int SEC_IN_MINUTE = 60;

		userInput = JOptionPane.showInputDialog("Please enter time in seconds:");
		timeInSeconds = Integer.parseInt(userInput);

		hours = timeInSeconds / SEC_IN_HOUR;
		timeInSeconds = timeInSeconds % SEC_IN_HOUR;

		minutes = timeInSeconds / SEC_IN_MINUTE;
		seconds = timeInSeconds % SEC_IN_MINUTE;

		output = String.format("%02d:%02d:%02d", hours, minutes, seconds);
		JOptionPane.showMessageDialog(null, output, "Time",
				JOptionPane.INFORMATION_MESSAGE);
	}
}