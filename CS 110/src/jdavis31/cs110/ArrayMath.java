package jdavis31.cs110;
import java.util.Scanner;

public class ArrayMath {
	public static int countEven(int[] array) {
		int nrEven = 0;
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0) {
				nrEven++; 
			}
		}
		
		return nrEven;
	}
	
	public static double[] cumulativeSum(double[] array) {
		double cumulation = 0;
		double[] cumulationArray = new double[array.length];
		
		for (int i = 0; i < array.length; i++) {
			cumulation += array[i];
			cumulationArray[i] = cumulation;
		}
		return cumulationArray;
	}
	
	public static double meanOfArray(double[] array) {
		return sumArray(array) / array.length;
	}
	
	public static double[] normalizeArray(double[] array) {
		double[] normalized = new double[array.length];
		
		for (int i = 0; i < array.length; i++) {
			normalized[i] = array[i] - meanOfArray(array);
		}
		
		return normalized;
	}
	
	public static void printArray(double[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			System.out.printf("%-10.2f ", numbers[i]);
		}
		
		System.out.println();
	}
	
	public static double sumArray(double[] array) {
		double sum = 0;
		
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum;
	}
	
	//Main method for testing my other methods.
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		final int NR_ELEMENTS = 5;
		int[] arrayInteger = new int[NR_ELEMENTS];
		double[] arrayDouble = new double[NR_ELEMENTS];
		
		System.out.println("Please enter " + NR_ELEMENTS + " numbers seperated by whitespace.");
		for (int i = 0; i < NR_ELEMENTS; i++) {
			arrayDouble[i] = input.nextDouble();
			arrayInteger[i] = (int)(arrayDouble[i]);
		}
		
		System.out.println("There are " + countEven(arrayInteger) + " even numbers in this array.");
		
		System.out.println("Normalized array:");
		printArray(normalizeArray(arrayDouble));
		
		System.out.println("Cumulative array:");
		printArray(cumulativeSum(arrayDouble));
		input.close();
	}
}
