import java.awt.Color;

public class GrowCircle extends BlinkCircle{
	private double speed;
	private int direction;
	private final double MAX_RADIUS = 0.5;
	private boolean blink;
	
	public GrowCircle(double x, double y, double radius, Color color1, Color color2) {
		super(x, y, radius, color1, color2);
		speed = 0.05;
		direction = 1;
		blink = true;
	}
	
	private void updateRadius(){
		if (radius >= MAX_RADIUS) direction = -1;
		else if (radius - speed <= 0) direction = 1;
		radius += direction * speed;
	}
	
	public void draw() {
		if (blink) {
			StdDraw.setPenColor(color2);
		}
		else {
			StdDraw.setPenColor(color1);
		}
		
		blink = !blink;
		StdDraw.filledCircle(x, y, radius);
		updateRadius();
	}
}
