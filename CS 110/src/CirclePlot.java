

public class CirclePlot {
	public static void main(String[] args) {
		final double MIN_X = -1;
		final double MAX_X = 2;
		final double MIN_Y = -1;
		final double MAX_Y = 2;
		double x = 0;
		int red = 0;
		
		StdDraw.setXscale(MIN_X, MAX_X);
		StdDraw.setYscale(MIN_Y, MAX_Y);
		
		StdDraw.setPenRadius(0.01);
		StdDraw.point(0, 0);
		
		StdDraw.setPenRadius();
		
		for (double radius = 0; radius <= 1; radius += 0.001) {
			StdDraw.setPenRadius(radius * 0.05);
			StdDraw.setPenColor(red, (int)(Math.random() * 255), (int)(Math.random() * 255));
			
			red = (int)(Math.random() * 255);
			
			x = Math.random() * (MAX_X - MIN_X) + MIN_X;
			StdDraw.point(x, Math.sin(x));
		}
	}
}
