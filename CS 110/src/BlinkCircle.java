import java.awt.Color;

public class BlinkCircle extends Circle{
	protected Color color1;
	protected Color color2;
	private boolean blink;
	
	public BlinkCircle(double x, double y, double radius, Color color1, Color color2){
		super(x, y, radius);
		this.color1 = color1;
		this.color2 = color2;
		blink = false;
	}
	
	public void draw() {
		if (blink) {
			StdDraw.setPenColor(color2);
		}
		else {
			StdDraw.setPenColor(color1);
		}
		
		blink = !blink;
		StdDraw.filledCircle(x, y, radius);
	}
}
